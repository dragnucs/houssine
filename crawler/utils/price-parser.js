module.exports = function parse(price) {
  if (!/^\d/.test(price) || !price || price <= 0) {
    return -1
  }

  let parsedPrice = price.replace(/( |mad|dhs|dh)/gi, '').trim()

  parsedPrice = /\.\d{1,2}$/.test(parsedPrice)
    ? parsedPrice.replace(/,/g, '')
    : parsedPrice.replace(/\./g, '').replace(',', '.')

  return Number.parseFloat(parsedPrice)
}
