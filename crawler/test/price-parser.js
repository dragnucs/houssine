const assert = require('assert')
const parse = require('../utils/price-parser.js')

describe('price-parser', function() {
  it('should return -1 when value is invalid price', function() {
    assert.equal(parse('lorem'), -1)
    assert.equal(parse('MAD 12'), -1)
    assert.equal(parse('0'), -1)
    assert.equal(parse(0), -1)
  })

  it('should return a number when value is valid price', function() {
    assert.equal(typeof parse('120.00 MAD'), 'number')
  })

  it('should return a positive number when value is valid price', function() {
    assert(parse('120.00 MAD' > 0))
  })

  it('should parse French style price', function() {
    assert.equal(parse('120,00 MAD'), 120.0)
    assert.equal(parse('1.120,03 MAD'), 1120.03)
    assert.equal(parse('14.120,00 MAD'), 14120.0)
    assert.equal(parse('123.120,50 MAD'), 123120.5)
    assert.equal(parse('1.234.567,00 MAD'), 1234567.0)
  })

  it('should parse Morrocan style price', function() {
    assert.equal(parse('120,00 MAD'), 120.0)
    assert.equal(parse('1 120,03 MAD'), 1120.03)
    assert.equal(parse('14 120,00 MAD'), 14120.0)
    assert.equal(parse('123 120,50 MAD'), 123120.5)
    assert.equal(parse('1 234 567,00 MAD'), 1234567.0)
  })

  it('should parse English style price', function() {
    assert.equal(parse('120.00 MAD'), 120.0)
    assert.equal(parse('1,120.03 MAD'), 1120.03)
    assert.equal(parse('14,120.00 MAD'), 14120.0)
    assert.equal(parse('123,120.50 MAD'), 123120.5)
    assert.equal(parse('1,234,567.00 MAD'), 1234567.0)
  })

  it('should parse unitless price', function() {
    assert.equal(parse('120.03'), 120.03)
    assert.equal(parse('130.50'), 130.5)
  })

  it('should parse MAD-suffixed price', function() {
    assert.equal(parse('120.00 MAD'), 120.0)
  })

  it('should parse Dhs-suffixed price', function() {
    assert.equal(parse('120.00 DHS'), 120.0)
    assert.equal(parse('120.00 Dhs'), 120.0)
  })

  it('should parse Dh-sufixed price', function() {
    assert.equal(parse('120.00 Dh'), 120.0)
  })

  it('should parse integer price', function() {
    assert.equal(parse('120 Dh'), 120.0)
  })
})
