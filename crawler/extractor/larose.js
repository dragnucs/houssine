const elasticsearch = require('elasticsearch')
const crawler = require('crawlerr')
const parser = require('../utils/price-parser.js')
const Sentry = require('@sentry/node')

Sentry.init({
  dsn: 'https://5e239e1bddd9494790dac41757882ea2@sentry.dragnucs.com/4'
})
Sentry.addBreadcrumb({ message: 'Larose' })

const client = new elasticsearch.Client({
  host: 'search:9200',
  log: 'debug'
})

const getProduct = function({ req, res, uri }) {
  const document = res.document

  const product = {
    id: new Buffer.from(uri).toString('base64'),
    '@timestamp': new Date().toISOString(),
    seller: 'Larose.ma',
    link: uri,
    title: document.querySelector('div.product_name h1').textContent.trim(),
    price: parser(
      document.querySelector('div.price span.prix_produit_left').textContent
    ),
    description: document.querySelector('div.produit_commun')
      ? document.querySelector('div.produit_commun').textContent.trim()
      : document
          .querySelector('div.col-lg-6:nth-child(8) > div:nth-child(1)')
          .textContent.trim(),
    thumbnail: document.querySelectorAll('.swiper-wrapper .swiper-slide img')[0]
      .attributes.src.value,
    specs: {
      brand: document.querySelectorAll('h2.titre_prd_associe_h2')
        ? document
            .querySelectorAll('h2.titre_prd_associe_h2')[1]
            .textContent.trim()
            .replace('Autres articles de la marque ', '')
        : '',
      category: document.querySelector('a.active_mere')
        ? document.querySelector('a.active_mere').textContent.trim()
        : ''
    }
  }

  console.log(product)

  client.index(
    {
      index: 'products',
      type: 'product',
      id: product.id,
      body: product
    },
    (err, res) => {
      console.error(err)
      console.log(res)
    }
  )
}

const spider = crawler('https://larose.ma')

spider.when('/femme/[all:slug]-[digit:id]-[digit:category].html', getProduct)
spider.on('error', error => console.error(`[Error] ${error}`))
spider.start()

module.exports = getProduct
