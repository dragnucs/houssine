const elasticsearch = require('elasticsearch')
const crawler = require('crawlerr')
const parser = require('../utils/price-parser.js')
const Sentry = require('@sentry/node')

Sentry.init({
  dsn: 'https://5e239e1bddd9494790dac41757882ea2@sentry.dragnucs.com/4'
})
Sentry.addBreadcrumb({ message: 'Tabtel' })

const client = new elasticsearch.Client({
  host: 'search:9200',
  log: 'info'
})

const getProduct = function({ req, res, uri }) {
  const document = res.document

  const product = {
    id: new Buffer.from(uri).toString('base64'),
    '@timestamp': new Date().toISOString(),
    seller: 'Tabtel.ma',
    link: uri,
    title: document
      .querySelector('#center_column [itemprop="name"]')
      .textContent.trim(),
    price: parser(
      document.querySelector('#center_column [itemprop="price"]').textContent
    ),
    description: document
      .querySelector('#center_column [itemprop="description"]')
      .textContent.trim(),
    thumbnail: document.querySelector('#center_column img[itemprop="image"]')
      .attributes.src.value,
    specs: {
      brand:
        document
          .querySelector('#center_column [itemprop="sku"]')
          .textContent.trim() || 'N/A',
      category: document.querySelector(
        '[itemtype="http://data-vocabulary.org/Breadcrumb"] [itemprop="title"]'
      ).textContent
    }
  }

  console.log(product)

  client.index(
    {
      index: 'products',
      type: 'product',
      id: product.id,
      body: product
    },
    (err, res) => {
      console.error(err)
      console.log(res)
    }
  )
}

const spider = crawler('http://tabtel.ma', {
  concurent: 1,
  interval: 2000
})

spider.when('/[all:lang]/[all:category]/[digit:id]-[all:slug].html', getProduct)
spider.on('error', error => console.error(`[Error] ${error}`))
spider.start()

module.exports = getProduct
