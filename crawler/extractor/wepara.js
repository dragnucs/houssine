const elasticsearch = require('elasticsearch')
const crawler = require('crawlerr')
const parser = require('../utils/price-parser.js')
const Sentry = require('@sentry/node')

Sentry.init({
  dsn: 'https://5e239e1bddd9494790dac41757882ea2@sentry.dragnucs.com/4'
})
Sentry.addBreadcrumb({ message: 'Wepara' })

const client = new elasticsearch.Client({
  host: 'search:9200',
  log: 'debug'
})

const getProduct = function({ req, res, uri }) {
  const document = res.document

  const product = {
    id: new Buffer.from(uri).toString('base64'),
    '@timestamp': new Date().toISOString(),
    seller: 'Wepara.ma',
    link: uri,
    title: document.querySelector('h1').textContent,
    price: parser(document.querySelector('p.BigPrice').firstChild.nodeValue),
    description: document.querySelector('#desc').textContent,
    thumbnail: document.querySelector('img.img-zoom.img-thumbnail').attributes
      .src.value,
    specs: {
      brand: document.querySelector(
        'div.product-details-color h5:nth-child(4) > a:nth-child(1)'
      ).textContent,
      category: document.querySelector(
        'div.product-details-color h5:nth-child(5) > a:nth-child(1)'
      ).textContent
    }
  }

  console.log(product)

  client.index(
    {
      index: 'products',
      type: 'product',
      id: product.id,
      body: product
    },
    (err, res) => {
      console.error(err)
      console.log(res)
    }
  )
}

const spider = crawler('https://www.wepara.ma/')
spider.when('/Products/Detail/[digit:id]', getProduct)
spider.when('/Products/Detail/[digit:id]/[all:slug]', getProduct)
spider.on('error', error => console.error(`[Error] ${error}`))
spider.start()

module.exports = getProduct
