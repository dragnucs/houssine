const elasticsearch = require('elasticsearch')
const crawler = require('crawlerr')
const parser = require('../utils/price-parser.js')
const Sentry = require('@sentry/node')

Sentry.init({
  dsn: 'https://5e239e1bddd9494790dac41757882ea2@sentry.dragnucs.com/4'
})
Sentry.addBreadcrumb({ message: 'Ultrapc' })

const client = new elasticsearch.Client({
  host: 'search:9200',
  log: 'info'
})

const getProduct = function({ req, res, uri }) {
  const document = res.document

  const product = {
    id: new Buffer.from(uri).toString('base64'),
    '@timestamp': new Date().toISOString(),
    seller: 'Ultrapc.ma',
    link: uri,
    title: document.querySelector('h1.product-title').textContent,
    price: parser(
      document.querySelector('div.current-price span.price').attributes.content
        .value
    ),
    description: document.querySelector('#description div.product-description')
      .textContent,
    thumbnail: document.querySelector('#product-images-modal img').attributes
      .src.value,
    specs: {
      brand: document.querySelector('div.product-manufacturer img').attributes
        .alt.value,
      category: document.querySelector(
        '#left-column div.block-categories p.block-title a'
      ).textContent
    }
  }

  console.log(product)

  client.index(
    {
      index: 'products',
      type: 'product',
      id: product.id,
      body: product
    },
    (err, res) => {
      console.error(err)
      console.log(res)
    }
  )
}

const spider = crawler('http://ultrapc.ma', {
  concurent: 1,
  interval: 2000
})

spider.when('/[all:category]/[digit:id]-[all:slug].html', getProduct)
spider.on('error', error => console.error(`[Error] ${error}`))
spider.start()

module.exports = getProduct
