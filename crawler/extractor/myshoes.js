const elasticsearch = require('elasticsearch')
const crawler = require('crawlerr')
const parser = require('../utils/price-parser.js')
const Sentry = require('@sentry/node')

Sentry.init({
  dsn: 'https://5e239e1bddd9494790dac41757882ea2@sentry.dragnucs.com/4'
})
Sentry.addBreadcrumb({ message: 'MyShoes' })

const client = new elasticsearch.Client({
  host: 'search:9200',
  log: 'debug'
})

const getProduct = function({ req, res, uri }) {
  const document = res.document

  const product = {
    id: new Buffer.from(uri).toString('base64'),
    '@timestamp': new Date().toISOString(),
    seller: 'MyShoes.ma',
    link: uri,
    title: document
      .querySelector('h1.product-single__title')
      .textContent.trim(),
    price: parser(
      document
        .querySelector('span.price-item[data-sale-price]')
        .textContent.trim()
    ),
    description: document
      .querySelector('div.product-single__description')
      .textContent.trim(),
    thumbnail: document.querySelector(
      '.product-single__photo-wrapper img.product-featured-img'
    ).attributes.src.value,
    specs: {
      brand: 'MyShoes.ma',
      category: req.param('category')
    }
  }

  console.log(product)

  client.index(
    {
      index: 'products',
      type: 'product',
      id: product.id,
      body: product
    },
    (err, res) => {
      console.error(err)
      console.log(res)
    }
  )
}

const spider = crawler('https://www.myshoes.ma', {
  concurrent: 1,
  interval: 2000,
  headers: {
    'User-Agent':
      'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0'
  }
})

spider.when('/collections/[all:category]/products/[all:slug]', getProduct)
spider.on('error', error => console.error(`[Error] ${error}`))
spider.on('request', url => console.log(`[Success] ${url}`))
spider.start()
