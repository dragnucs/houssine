const elasticsearch = require('elasticsearch')
const crawler = require('crawlerr')
const parser = require('../utils/price-parser.js')
const Sentry = require('@sentry/node')

Sentry.init({
  dsn: 'https://5e239e1bddd9494790dac41757882ea2@sentry.dragnucs.com/4'
})
Sentry.addBreadcrumb({ message: 'Mapara' })

const client = new elasticsearch.Client({
  host: 'search:9200',
  log: 'debug'
})

const getProduct = function({ req, res, uri }) {
  const document = res.document

  const product = {
    id: new Buffer.from(uri).toString('base64'),
    '@timestamp': new Date().toISOString(),
    seller: 'Mapara.ma',
    link: uri,
    title: document.querySelector('.detail-desc strong').textContent,
    price: parser(document.querySelector('.detail-price').textContent),
    description: document.querySelector('#tabs-1').textContent,
    thumbnail: document.querySelector('.detail-img img').attributes.src.value,
    specs: {
      brand: document.querySelector('.detail-logo img').attributes.alt.value,
      category: Array.from(
        document.querySelector('.breadcrumb').querySelectorAll('li')
      ).map(item => item.textContent.trim())
    }
  }

  console.log(product)

  client.index(
    {
      index: 'products',
      type: 'product',
      id: product.id,
      body: product
    },
    (err, res) => {
      console.error(err)
      console.log(res)
    }
  )
}

const spider = crawler('http://mapara.ma')
spider.when('/produit/[digit:id]/[all:slug]', getProduct)
spider.on('error', error => console.error(`[Error] ${error}`))
spider.start()

module.exports = getProduct
