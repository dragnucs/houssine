const express = require('express')
const elasticsearch = require('elasticsearch')
const MatomoTracker = require('matomo-tracker')
const baseUri = `https://api.${process.env.DOMAIN_NAME}`

const matomo = new MatomoTracker(
  1,
  `https://analytics.${process.env.DOMAIN_NAME}/piwik.php`
)

const client = new elasticsearch.Client({
  host: 'search:9200',
  log: 'info'
})

const app = express()

const Sentry = require('@sentry/node')

Sentry.init({
  dsn: 'https://efdcc69c95f343dc8239e71ce339c780@sentry.dragnucs.com/2'
})
app.use(Sentry.Handlers.requestHandler())
app.use(Sentry.Handlers.errorHandler())

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  next()
})

app.get('/', (req, res) => {
  res.send('API Index.')
})

app.get(`/keywords/:keywords`, (req, res) => {
  client
    .search({
      index: 'products',
      from: req.query.page ? (req.query.page - 1) * 10 : 0,
      size: 10,
      body: {
        query: {
          bool: {
            should: {
              match: {
                title: {
                  query: req.params.keywords,
                  boost: 10
                }
              },
              match: {
                description: {
                  query: req.params.keywords,
                  boost: 6
                }
              }
            }
          }
        }
      }
    })
    .then(response => {
      res.send({
        total: response.hits.total,
        products: response.hits.hits.map(hit => hit._source)
      })
    })
    .catch(error => {
      throw new Error(res.sentry)
    })

  matomo.track({
    url: req.originalUrl,
    action_name: 'API Search',
    search: req.params.keywords,
    ua: req.get('User-Agent')
  })
})

const port = 4000

app.listen(port, () => console.log(`Listening on port ${port}.`))
