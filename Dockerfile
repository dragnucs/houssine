FROM node:11.4.0-slim

COPY . /usr/src/app
WORKDIR /usr/src/app
RUN yarn global add node-sass \
  && yarn && yarn build

ENV HOST 0.0.0.0
EXPOSE 3000
EXPOSE 4000

CMD ["npm" "start"]
